# API USUARIO #

API CRUD USUARIOS

### Recursos ###

* JAX-RS 2.0
* Hibernate 5.1
* Weblogic 12c
* MYSQL

### Servicios ###

* GET http://SERVER:IP/api-users/users/ devuelve todos los usuarios
* POST http://SERVER:IP/api-users/users/ Crea usuario
* POST http://SERVER:IP/api-users/users/update/USER_ID Actualiza un usuario
* GET http://SERVER:IP/api-users/users/delete/USER_ID borra un usuario

### Configuraciones ###

* Crear el datasoruce con JDNI jdbc/mysql
* Crear una base de datos test y una tabla usuario
* desplegar el war generado en el servidor
