package com.test.formaters;

import java.util.Date;

import com.test.database.User;
import com.test.models.SaveUserRequest;

public class UserFormater {
	static public User formatUser(SaveUserRequest user) {
		User userModel = new User();
		userModel.setCity(user.getCity());
		userModel.setCountry(user.getCountry());
		userModel.setDateCreated(new Date());
		userModel.setFristName(user.getFristName());
		userModel.setGender(user.getGender());
		userModel.setLastName(user.getLastName());
		return userModel;
	}

}
