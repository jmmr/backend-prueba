package com.test.dao;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;

import com.test.config.HibernateConfig;
import com.test.exception.SQLException;

public class BaseDAO {
	
	protected HibernateConfig hc;
    private Logger log = Logger.getLogger(BaseDAO.class);
    public boolean exist(String table,String field,String value) throws SQLException {
        try {
            String hql = String.format("select 1 from %s where %s = :val",table,field);
            List<?> list = hc.buildSession().createQuery(hql)
                    .setParameter("val",value)
                    .setMaxResults(1).list();
            if(list.isEmpty()) {
                return false;
            }
            return true;
        }catch(Exception ex) {
            log.error("SQLException ",ex);
            throw new SQLException("SQLException",ex);
        }
    }
    
    public String getRecord(String table,String selectField,String whereField,String value) throws SQLException {
        try {
            
            String hql = String.format("select %s from %s where %s = :val",selectField,table,whereField);
            List<Object[]> list = hc.buildSession().createQuery(hql)
                    .setParameter("val",value)
                    .setMaxResults(1).list();
            if(list.isEmpty()) {
                return null;
            }
            String val = null;
            return (val = (String)(list.get(0))[0]).isEmpty() ? null : val;
        }catch(Exception ex) {
            log.error("SQLException ",ex);
            throw new SQLException("SQLException",ex);
        }
    }
    
    public BigDecimal getNextSequence(String sequenceName) throws SQLException {
        try {
            
            String hql = String.format("select %s.NEXTVAL from DUAL",sequenceName);
            List<BigDecimal> list = hc.buildSession().createSQLQuery(hql).list();
            if(list.isEmpty()) {
                return null;
            }
            String val = null;
            return list.get(0);
        }catch(Exception ex) {
            log.error("SQLException ",ex);
            throw new SQLException("SQLException",ex);
        }
    }
    
    public <T>Integer save(T entity) throws SQLException {
        Integer entityID = null;
        try {
            return entityID = (Integer) hc.buildSession().save(entity);
        }catch(Exception ex) {
            log.error("SQLException ",ex);
            throw new SQLException("SQLException",ex);
        }
    }
    
    public void commit() throws SQLException {
        try {
            hc.commitTransacion();
        }catch(Exception ex) {
            log.error("SQLException ",ex);
            ex.printStackTrace();
            throw new SQLException("SQLException",ex);
        }
    }

}
