package com.test.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import com.test.config.Constants;
import com.test.config.HibernateConfig;
import com.test.database.User;
import com.test.exception.HibernateConnectionException;
import com.test.exception.NotFoundAnnotatedClassException;
import com.test.exception.SQLException;
import com.test.formaters.UserFormater;
import com.test.models.SaveUserRequest;
import com.test.models.UserListResponse;

public class UserDAO extends BaseDAO implements Constants{
	private Logger log = Logger.getLogger(UserDAO.class);
    public UserDAO() {
        hc = new HibernateConfig();
        hc.addClass(User.class);
    }
	public UserListResponse getAllUsers(){
		UserListResponse response = new UserListResponse();
		String hql = "from User ";
        try {
			List<User> list = hc.buildSession().createQuery(hql).list();
			if(list == null || list.isEmpty()) {
				response.setMsg(NO_USERS_FOUND_MSG);
				response.setRc(NO_USERS_FOUND_RC);
				response.setUsers(new ArrayList());
				return response;
			}
			response.setUsers(new ArrayList(list));
			response.setMsg(SUCCESS_MSG);
			response.setRc(SUCCESS_RC);
		} catch (HibernateException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} catch (NotFoundAnnotatedClassException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} catch (HibernateConnectionException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		}
		return response;
	}
	public UserListResponse saveUser(User user){
		UserListResponse response = new UserListResponse();
        try {
        	this.save(user);
        	this.commit();
			response.setMsg(SUCCESS_MSG);
			response.setRc(SUCCESS_RC);
		} catch (HibernateException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} catch (SQLException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} 
		return response;
	}
	
	public UserListResponse updateUser(SaveUserRequest user, int id){
		UserListResponse response = new UserListResponse();
		String hql = "from User where id = :id";
        try {
			List<User> list = hc.buildSession().createQuery(hql).setParameter("id",id).list();
			if(list == null || list.isEmpty()) {
				response.setMsg(NO_USERS_FOUND_MSG);
				response.setRc(NO_USERS_FOUND_RC);
				response.setUsers(new ArrayList());
				return response;
			}
			
			User newUser = list.get(0);
			newUser.setCity(user.getCity());
			newUser.setCountry(user.getCountry());
			newUser.setFristName(user.getFristName());
			newUser.setGender(user.getGender());
			newUser.setLastName(user.getLastName());
			hc.buildSession().update(newUser);
			commit();
			response.setMsg(SUCCESS_MSG);
			response.setRc(SUCCESS_RC);
		} catch (HibernateException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} catch (NotFoundAnnotatedClassException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} catch (HibernateConnectionException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		}catch (SQLException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		}
		return response;
	}
	
	public UserListResponse deleteUser(int id){
		UserListResponse response = new UserListResponse();
		String hql = "from User where id = :id";
        try {
			List<User> list = hc.buildSession().createQuery(hql).setParameter("id",id).list();
			if(list == null || list.isEmpty()) {
				response.setMsg(NO_USERS_FOUND_MSG);
				response.setRc(NO_USERS_FOUND_RC);
				response.setUsers(new ArrayList());
				return response;
			}
			hc.buildSession().delete(list.get(0));
			commit();
			response.setMsg(SUCCESS_MSG);
			response.setRc(SUCCESS_RC);
		} catch (HibernateException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} catch (NotFoundAnnotatedClassException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		} catch (HibernateConnectionException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		}catch (SQLException e) {
			response.setMsg(SERVICE_ERROR_MSG);
			response.setRc(SERVICE_ERROR_RC);
			e.printStackTrace();
		}
		return response;
	}
}
