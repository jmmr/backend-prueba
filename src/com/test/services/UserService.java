package com.test.services;

import com.test.dao.UserDAO;
import com.test.database.User;
import com.test.formaters.UserFormater;
import com.test.models.SaveUserRequest;
import com.test.models.UserListResponse;

public class UserService {
	public UserListResponse getUsersList() {
		UserDAO userDAO = new UserDAO();
		return userDAO.getAllUsers();
		
	}
	public UserListResponse saveUser(SaveUserRequest userRequest) {
		UserDAO userDAO = new UserDAO();
		return userDAO.saveUser(UserFormater.formatUser(userRequest));
	}
	public UserListResponse updateUser(SaveUserRequest userRequest,int id) {
		UserDAO userDAO = new UserDAO();
		return userDAO.updateUser(userRequest, id);
	}
	public UserListResponse deleteUser(int id) {
		UserDAO userDAO = new UserDAO();
		return userDAO.deleteUser(id);
	}
}
