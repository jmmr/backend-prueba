package com.test.config;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.jersey.api.client.filter.LoggingFilter;
import com.test.resource.Resource;
@ApplicationPath("")
public class AppConfig extends Application {
	@Override
    public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>() {{
            // Add your resources.
            add(Resource.class);

            // Add LoggingFilter.
            add(LoggingFilter.class);
        }};
    }
}
