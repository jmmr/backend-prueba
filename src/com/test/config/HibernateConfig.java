package com.test.config;



import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.test.config.Constants;
import com.test.exception.HibernateConnectionException;
import com.test.exception.NotFoundAnnotatedClassException;

import org.apache.log4j.Logger;
import java.util.ArrayList;


public class HibernateConfig implements Constants {
    private final Logger log = Logger.getLogger(HibernateConfig.class.getName());
    private static SessionFactory sessionFactory;
    private static Session session;
    private static Transaction transaction;
    private Configuration configuration = new Configuration();
    private ArrayList<Class> classList = new ArrayList();
    private final SessionFactory getSession() throws NotFoundAnnotatedClassException,HibernateConnectionException{
        try {
                if(classList.isEmpty()) {
                    log.error("No defined Annotated Clases");
                    throw new NotFoundAnnotatedClassException();
                }
                Properties props = new Properties();
                props.put(Environment.DATASOURCE,JDNI_ORACLE);
                props.put(Environment.POOL_SIZE,"10");
                props.put(Environment.SHOW_SQL,"true");
                props.put(Environment.DIALECT,"org.hibernate.dialect.MySQLDialect");
                configuration.setProperties(props);
                for(Class temp: classList) {
                    configuration.addAnnotatedClass(temp);
                }
                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build();
                return sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }catch(Exception ex) {
            log.error("Exception error",ex);
            ex.printStackTrace();
            throw new HibernateConnectionException();
        }
    }
    
    public final HibernateConfig addClass(Class t) {
        classList.add(t);
        return this;
    }
    
    private SessionFactory buildSessionFactory() throws NotFoundAnnotatedClassException,HibernateConnectionException {
        return sessionFactory == null ? getSession() : sessionFactory;
    }
    
    public Session buildSession() throws HibernateException, NotFoundAnnotatedClassException, HibernateConnectionException {
        if(session == null || !session.isOpen()) {
            session = buildSessionFactory().openSession();
            transaction = session.beginTransaction();
        }
        return session;
    }
    
    public Session generateNewSession() throws HibernateException, NotFoundAnnotatedClassException, HibernateConnectionException {
        if(session != null && transaction != null) {
            if(session.isOpen()) {
                session.close();
                transaction = null;
                session = null;
            }
        }
        Session session = buildSessionFactory().openSession();
        transaction = session.beginTransaction();
        return session;
    }
    
    public void commitTransacion() throws HibernateException, NotFoundAnnotatedClassException, HibernateConnectionException {
        if(transaction != null) {
        	transaction.commit();
        	generateNewSession();
        }
    }
    
    public void rollback() {
        if(session != null && transaction != null && session.isOpen()) {
            if(transaction.isActive())transaction.rollback();
        }
    }
    
    protected void finalize() throws Throwable { 
        
    } 
}

