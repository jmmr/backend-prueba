package com.test.config;

public interface Constants {
	String JDNI_ORACLE = "jdbc/mysql";
	String NO_USERS_FOUND_MSG = "Sin usuarios que mostrara";
	String NO_USERS_FOUND_RC = "05";
	String SUCCESS_MSG = "Ok";
	String SUCCESS_RC = "00";
	String SERVICE_ERROR_MSG = "Service not available";
	String SERVICE_ERROR_RC = "-100";
}
