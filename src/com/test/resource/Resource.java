package com.test.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.test.models.SaveUserRequest;
import com.test.services.UserService;

@Path("/users")
public class Resource {

	@GET()
	@Produces({ "application/json" })
	public Response getUsers() {
		return Response.ok().entity(new UserService().getUsersList()).build();
		
	}
	@POST
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	public Response saveUser(SaveUserRequest user) {
		return Response.ok().entity(new UserService().saveUser(user)).build();
	}
	@POST
	@Path("/update/{id}")
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	public Response updateUser(SaveUserRequest user,@PathParam("id") int id) {
		return Response.ok().entity(new UserService().updateUser(user, id)).build();
	}
	@GET
	@Path("/delete/{id}")
	@Produces({ "application/json" })
	public Response deleteUser(@PathParam("id") int id) {
		return Response.ok().entity(new UserService().deleteUser(id)).build();
	}

}
