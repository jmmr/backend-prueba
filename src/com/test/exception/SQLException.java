package com.test.exception;

public class SQLException extends Exception {
	public SQLException() {
		super();
	}
	public SQLException(String message,Throwable ex) {
		super(message,ex);
	}
}
