package com.test.exception;

public class HibernateConnectionException extends Exception{
	public HibernateConnectionException() {
		
	}
	public HibernateConnectionException(String message,Throwable ex) {
		super(message,ex);
	}
}
