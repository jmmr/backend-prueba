package com.test.exception;

public class NotFoundAnnotatedClassException extends Exception{
	public NotFoundAnnotatedClassException() {}
	public NotFoundAnnotatedClassException(String message,Throwable ex) {
		super(message,ex);
	}
}
