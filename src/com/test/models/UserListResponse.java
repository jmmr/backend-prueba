package com.test.models;

import java.util.ArrayList;

import com.test.database.User;

public class UserListResponse {
	String rc;
	String msg;
	ArrayList<User> users;
	public String getRc() {
		return rc;
	}
	public void setRc(String rc) {
		this.rc = rc;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ArrayList<User> getUsers() {
		return users;
	}
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
	
}
